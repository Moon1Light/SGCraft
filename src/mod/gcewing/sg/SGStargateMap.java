package gcewing.sg;

import java.util.*;

import net.minecraft.nbt.*;
import net.minecraft.world.*;
import net.minecraftforge.common.util.Constants;

public class SGStargateMap  extends WorldSavedData {

  public SGStargateMap(String name) {
    super(name);
  }

  private Map<Integer, Set<ChunkCoordIntPair>> knownStargates = new HashMap<>();;

  public static SGStargateMap get() {
    World world = BaseUtils.getWorldForDimension(0);
    return BaseUtils.getWorldData(world, SGStargateMap.class, "sgcraft:stargate_map");
  }

  public static void add(int dimension, ChunkCoordIntPair chunkCoord) {
    get()._add(dimension, chunkCoord);
  }

  private void _add(int dimension, ChunkCoordIntPair chunkCoord) {
    final Set<ChunkCoordIntPair> chunks = knownStargates
        .computeIfAbsent(dimension, k -> new HashSet<>());
    if (!chunks.contains(chunkCoord)) {
      chunks.add(chunkCoord);
      markDirty();
      if (SGAddressing.debugAddressing) {
        System.out.printf("SGAddressing.reg: (%s, %s)\n", chunkCoord.chunkXPos, chunkCoord.chunkZPos);
      }
    }
  }

  public static void remove(int dimension, ChunkCoordIntPair chunkCoord) {
    get()._remove(dimension, chunkCoord);
  }

  public void _remove(int dimension, ChunkCoordIntPair chunkCoord) {
    final Set<ChunkCoordIntPair> chunks = knownStargates.get(dimension);
    if (chunks != null && chunks.contains(chunkCoord)) {
      chunks.remove(chunkCoord);
      markDirty();
      if (SGAddressing.debugAddressing) {
        System.out.printf("SGAddressing.unreg: (%s, %s)\n", chunkCoord.chunkXPos, chunkCoord.chunkZPos);
      }
    }
  }

  public static boolean isExists(int dimension, ChunkCoordIntPair chunkCoord) {
    final SGStargateMap sgmap = get();
    final Set<ChunkCoordIntPair> chunks = sgmap.knownStargates.get(dimension);
    return chunks != null && chunks.contains(chunkCoord);
  }

  @Override
  public void readFromNBT(NBTTagCompound nbtTagCompound) {
    knownStargates = new HashMap<>();
    NBTTagList stargatesChunkList = nbtTagCompound.getTagList("Stargates", Constants.NBT.TAG_COMPOUND);
    for (int i = 0; i < stargatesChunkList.tagCount(); i++) {
      NBTTagCompound stargateData = stargatesChunkList.getCompoundTagAt(i);
      final int dim = stargateData.getInteger("dim");
      final int x = stargateData.getInteger("x");
      final int z = stargateData.getInteger("z");
      _add(dim, new ChunkCoordIntPair(x, z));
    }
  }

  @Override
  public void writeToNBT(NBTTagCompound nbtTagCompound) {
    NBTTagList stargatesChunkList = new NBTTagList();
    nbtTagCompound.setTag("Stargates", stargatesChunkList);
    knownStargates.forEach((dimension, chunks) -> {
      chunks.forEach(chunkCoord -> {
        NBTTagCompound stargateData = new NBTTagCompound();
        stargatesChunkList.appendTag(stargateData);
        stargateData.setInteger("dim", dimension);
        stargateData.setInteger("x", chunkCoord.chunkXPos);
        stargateData.setInteger("z", chunkCoord.chunkZPos);
      });
    });
  }
}
